#include <stdio.h>

int main(int argc, char const *argv[]) {
  int a=0;
  float salario=0.0f;

  printf("¿Cuantas piezas vendiste?: ");
  scanf("%d",&a );
  if (a>=1.0 && a<=12.0) {
    salario=(450.0*0.05*a);
  } else {
    if (a<24.0 && a>=13.0) {
      salario=(450.0*0.07* a);
    } else {
      if (a>24.0 && a<=30.0) {
        salario=(435*a*0.07);
      } else {
        if (a>30.0) {
          salario=(435.0*0.1*a);
        }
      }
    }
  }

  printf("Tu sueldo es: $%f\n", salario );
  return 0;
}
